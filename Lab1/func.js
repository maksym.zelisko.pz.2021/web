let rowCount = 1;

var tableBody = document.getElementById("tableBody");
var createStud = document.getElementById("createStud");
var closeAdd = document.getElementById("closeAdd");

var deleteBtn = document.getElementById("deleteBtn");
var canselBtn = document.getElementById("canselBtn");

canselBtn.onclick = function()
{
    var popup = document.getElementById("deleteStud");
        popup.classList.toggle("show");
}

closeAdd.onclick = function()
{
    var popup = document.getElementById("addStud");
    popup.classList.toggle("show");
}

createStud.onclick = function()
{
    var startTabB = document.createElement("tr");
    startTabB.id = "row" + rowCount;

    //checkbox
    var tempTd = document.createElement("td");
    var inputT = document.createElement("input");
    inputT.type = "checkbox";
    inputT.id = "checkbox";
    tempTd.appendChild(inputT);
    startTabB.appendChild(tempTd);

    //group
    tempTd = document.createElement("td");
    tempTd.textContent = "";
    startTabB.appendChild(tempTd);

    //name
    tempTd = document.createElement("td");
    tempTd.textContent = "";
    startTabB.appendChild(tempTd);

    //gender
    tempTd = document.createElement("td");
    tempTd.textContent = "";
    startTabB.appendChild(tempTd);

    //birthday
    tempTd = document.createElement("td");
    tempTd.textContent = "";
    startTabB.appendChild(tempTd);
    
    //status
    tempTd = document.createElement("td");
    var divTemp = document.createElement("div");
    divTemp.classList.add("round");
    tempTd.appendChild(divTemp);
    startTabB.appendChild(tempTd);

    //options
    tempTd = document.createElement("td");
    var buttonTemp = document.createElement("button");
    buttonTemp.textContent = "Edit";
    tempTd.appendChild(buttonTemp);

    buttonTemp = document.createElement("button");
    buttonTemp.textContent = "Remove";
    buttonTemp.onclick = function()
    {
        var popup = document.getElementById("deleteStud");
        popup.classList.toggle("show");
        deleteBtn.onclick = function()
        {
            tableBody.removeChild(startTabB);
            var popup = document.getElementById("deleteStud");
            popup.classList.toggle("show");
        }
    }   

    tempTd.appendChild(buttonTemp);

    startTabB.appendChild(tempTd);
    tableBody.appendChild(startTabB);

    var popup = document.getElementById("addStud");
    popup.classList.toggle("show");
}

function profilePop() 
{
    var popup = document.getElementById("profilePop");
    popup.classList.toggle("show");
}

function notificPop()
{
    var popup = document.getElementById("notificPop");
    popup.classList.toggle("show");
}

function addStud()
{
    var popup = document.getElementById("addStud");
    popup.classList.toggle("show");
}