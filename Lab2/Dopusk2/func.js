var FirstName = document.getElementById("FirstName");
var LastName = document.getElementById("LastName");
var Uni = document.getElementById("Uni");
var Course = document.getElementById("Course");
var Email = document.getElementById("Email");
var PhNumber = document.getElementById("PhNumber");
var Questions = document.getElementById("Questions");

var CheckC = document.getElementById("C");
var CheckCPP = document.getElementById("CPP");
var CheckCS = document.getElementById("CS");
var CheckJava = document.getElementById("Java");
var CheckJavaScript = document.getElementById("JS");

var RegisterBtn = document.getElementById("Register");
var DateOfStart = document.getElementById("DateOfStart");

RegisterBtn.onclick = function()
{
    FirstName.style.backgroundColor = "white";
    LastName.style.backgroundColor = "white";
    Uni.style.backgroundColor = "white"; 
    Course.style.backgroundColor = "white";
    Email.style.backgroundColor = "white";
    PhNumber.style.backgroundColor = "white";

    var Reg = /^[a-zA-Z]+$/;
    var RegPhone = /^[0-9]+$/;
    var RegEmail = /.ru+$/;

    if(!Reg.test(FirstName.value))
    {
        FirstName.style.backgroundColor = "red";
        alert("Wrong firstname!");
        return false;
    }
    else if(!Reg.test(LastName.value))
    {
        LastName.style.backgroundColor = "red";
        alert("Wrong lastname!");
        return false;
    }
    else if(!Reg.test(Uni.value))
    {
        Uni.style.backgroundColor = "red";
        alert("Wrong university!");
        return false;
    }
    else if(Course.value == "0")
    {
        Course.style.backgroundColor = "red";
        alert("Wrong course!");
        return false;
    }
    else if(RegEmail.test(Email.value) || Email.value == "")
    {
        Email.style.backgroundColor = "red";
        alert("Wrong email!");
        return false;
    }
    else if(!RegPhone.test(PhNumber.value))
    {
        PhNumber.style.backgroundColor = "red";
        alert("Wrong phone number!");
        return false;
    }
    else if(!CheckC.checked && !CheckCPP.checked && !CheckCS.checked && !CheckJava.checked && !CheckJavaScript.checked)
    {
        var popup = document.getElementById("showForm");
        popup.classList.toggle("show");
    }
    else
    {
        alert("You are registered succesfully!");
    }
    
    return false;
}

function pop()
{
    var popup = document.getElementById("showForm");
    popup.classList.toggle("show");
}

function okay()
{
    var now = new Date();
    if(DateOfStart.value == "")
    {
        alert("Wrong date!");
        return false;
    }
    var dat = new Date(DateOfStart.value);
    if(now > dat)
    {
        alert("Wrong date!");
        return false;
    }
    CheckCPP.checked = true;
    pop();
    alert("You are registered succesfully!");
    return 0;
}
