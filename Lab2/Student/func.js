let rowCount = 1;
let tempRow = 0;
let addOrEdit = 0;

var tableBody = document.getElementById("tableBody");
var createStud = document.getElementById("createStud");
var closeAdd = document.getElementById("closeAdd");

var deleteBtn = document.getElementById("deleteBtn");
var canselBtn = document.getElementById("canselBtn");

class students 
{
    constructor(id ,name, group, gender, birthday) 
    {
        this.id = id;
        this.name = name;
        this.group = group;
        this.gender = gender;
        this.birthday = birthday;
        this.status = 0;
    }
}

createStud.onclick = function()
{
    if(addOrEdit == 0)
    {
        createStudent();
    }
    else
    {
        editStudent();
    }
}

function editStudent() 
{
    var changeName = document.getElementById("setName");
    var changeGroup = document.getElementById("setGroup");
    var changeGenderM = document.getElementById("setGenderM");
    var changeGenderW = document.getElementById("setGenderW");
    var changeBirthday = document.getElementById("setBirthday");

    var cellName = document.getElementById("name"+tempRow);
    var cellGroup = document.getElementById("group"+tempRow);
    var cellGender = document.getElementById("gender"+tempRow);
    var cellBirthday = document.getElementById("birthday"+tempRow);

    console.log("name"+tempRow);

    var Reg = /^[a-zA-Z]+$/;
    var tempDate = Date(changeBirthday.value);
    var nowDate = Date();

    if(changeGenderM.checked && changeGenderW.checked)
    {
        alert("Wrong gender!");
        return false;
    }
    else if(!changeGenderM.checked && !changeGenderW.checked)
    {
        alert("Wrong gender!");
        return false;
    }
    else if(!Reg.test(changeName.value))
    {
        alert("Wrong name!");
        return false;
    } 
    else if(nowDate < tempDate)
    {
        alert("Wrong date!");
        return false;
    }

    cellName.textContent = changeName.value;
    cellGroup.textContent = changeGroup.value;
    cellBirthday.textContent = changeBirthday.value;
    if(changeGenderW.checked)
    {
        cellGender.textContent = "Woman";
    }
    else
    {
        cellGender.textContent = "Man";   
    }

    var popup = document.getElementById("addStud");
    popup.classList.toggle("show");

    const temp = new students(changeName.value, changeGroup.innerText, "M");
    if(changeGenderW.checked)
    {
        temp.gender = "W";
    }
    const jsonData = JSON.stringify(temp);
    console.log(jsonData);

    changeName.value = "";
    changeGroup.value = "";
    changeGenderM.checked = false;
    changeGenderW.checked = false;
    changeBirthday.value = "";
}

canselBtn.onclick = function()
{
    var popup = document.getElementById("deleteStud");
        popup.classList.toggle("show");
}

closeAdd.onclick = function()
{
    var popup = document.getElementById("addStud");
    popup.classList.toggle("show");
}

function createStudent()
{
    var setName = document.getElementById("setName");
    var setGroup = document.getElementById("setGroup");
    var setGenderM = document.getElementById("setGenderM");
    var setGenderW = document.getElementById("setGenderW");
    var setBirthday = document.getElementById("setBirthday");
    var elementId = document.getElementById("elementId");

    var Reg = /^[a-zA-Z]+$/;
    var tempDate = Date(setBirthday.value);
    var nowDate = Date();

    if(setGenderM.checked && setGenderW.checked)
    {
        alert("Wrong gender!");
        return false;
    }
    else if(!setGenderM.checked && !setGenderW.checked)
    {
        alert("Wrong gender!");
        return false;
    }
    else if(!Reg.test(setName.value))
    {
        alert("Wrong name!");
        return false;
    } 
    else if(nowDate < tempDate)
    {
        alert("Wrong date!");
        return false;
    }

    var startTabB = document.createElement("tr");
    startTabB.rowIndex = rowCount;

    //checkbox
    var tempTd = document.createElement("td");
    var inputT = document.createElement("input");
    inputT.type = "checkbox";
    inputT.id = "checkbox"+rowCount;
    tempTd.appendChild(inputT);
    startTabB.appendChild(tempTd);

    //group
    tempTd = document.createElement("td");
    tempTd.textContent = setGroup.textContent;
    tempTd.id = "group"+rowCount;
    startTabB.appendChild(tempTd);

    //name
    tempTd = document.createElement("td");
    tempTd.textContent = setName.value;
    tempTd.id = "name"+rowCount;
    startTabB.appendChild(tempTd);

    //gender
    tempTd = document.createElement("td");
    if(setGenderM.checked && setGenderW.checked)
    {
        alert("Wrong gender!");
        return false;
    }
    else if(setGenderW.checked)
    {
        tempTd.textContent = "Woman";
    }
    else if(setGenderM.checked)
    {
        tempTd.textContent = "Man";   
    }
    else
    {
        alert("Wrong gender!");
        return false;
    }
    tempTd.id = "gender"+rowCount;
    startTabB.appendChild(tempTd);

    //birthday
    tempTd = document.createElement("td");
    tempTd.textContent = setBirthday.value;
    tempTd.id = "birthday"+rowCount;
    startTabB.appendChild(tempTd);
    
    //status
    tempTd = document.createElement("td");
    var divTemp = document.createElement("div");
    divTemp.classList.add("round");
    tempTd.id = "status"+rowCount;
    tempTd.appendChild(divTemp);
    startTabB.appendChild(tempTd);

    //options

    //Edit
    tempTd = document.createElement("td");
    var buttonTemp = document.createElement("button");
    buttonTemp.textContent = "Edit";
    buttonTemp.id = "editStudBtn";
    buttonTemp.onclick = function()
    {
        var popup = document.getElementById("addStud");
        popup.classList.toggle("show");
        tempRow = this.parentNode.parentNode.rowIndex;
        addOrEdit = 1;
    }
    tempTd.appendChild(buttonTemp);


    //Remove
    buttonTemp = document.createElement("button");
    buttonTemp.textContent = "Remove";
    buttonTemp.onclick = function()
    {
        var popup = document.getElementById("deleteStud");
        popup.classList.toggle("show");
        deleteBtn.onclick = function()
        {
            tableBody.removeChild(startTabB);
            var popup = document.getElementById("deleteStud");
            popup.classList.toggle("show");
        }
    }
    //id
    elementId.value = rowCount;

    tempTd.appendChild(buttonTemp);

    startTabB.appendChild(tempTd);
    tableBody.appendChild(startTabB);

    var popup = document.getElementById("addStud");
    popup.classList.toggle("show");

    rowCount++;
    
    const temp = new students(elementId.value,setName.value, setGroup.innerText, "M");
    if(setGenderW.checked)
    {
        temp.gender = "W";
    }
    const jsonData = JSON.stringify(temp);
    console.log(jsonData);

    setName.value = "";
    setGroup.value = "";
    setGenderM.checked = false;
    setGenderW.checked = false;
    setBirthday.value = "";
    elementId.value = "";
}

function profilePop() 
{
    var popup = document.getElementById("profilePop");
    popup.classList.toggle("show");
}

function notificPop()
{
    var popup = document.getElementById("notificPop");
    popup.classList.toggle("show");
}

function addStud()
{
    var popup = document.getElementById("addStud");
    popup.classList.toggle("show");
    addOrEdit = 0;
}

window.addEventListener('load', async () =>{
  if('serviceWorker' in navigator)
  {
    try 
    {
      const reg = await navigator.serviceWorker.register('/mod.js');
      console.log("SW success", reg);
    } 
    catch (error) 
    {
      console.log("SW fail");
    }
}
});
