const staticCache = 'student-v2';
const dynamicCache = 'd-student-v2';
const assetUrls = 
[
    '/struct.html',
    '/func.js',
    '/fancy.css',
    '/ofline.html'
];
  
  self.addEventListener('install', event => 
  {
    event.waitUntil(
      caches.open(staticCache).then(cache => cache.addAll(assetUrls))
    );
  });

  self.addEventListener('activate', async event =>
  {
    const cacheNames = await caches.keys();
    await Promise.all(
      cacheNames.filter(name => name !== staticCache).filter(name => name !== dynamicCache)
      .map(name => caches.delete(name))
    )
  });
  
self.addEventListener('fetch', event => 
{
  const {request} = event;
  const url = new URL(request.url)
  if(url.origin === location.origin)
  {
    event.respondWith(cacheFirst(request));
  }
  else
  {
    event.respondWith(netWorkFirst(request));
  }
 
});

async function cacheFirst(request)
{
  const cached = await caches.match(request);
  return cached ?? await fetch(request);
}
async function netWorkFirst(request)
{
  const cache = await caches.open(dynamicCache);
  try 
  {
      const response = await fetch(request);
      cache.put(request, response.clone());
      return response;
  }
  catch(e)
  {
    const cached = await cache.match(request);
    return cached ?? cache.match('/ofline.html');
  }
}
